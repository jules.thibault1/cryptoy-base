from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    return [((a * i + b) % n) for i in range(n)]


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    result = [0] * n

    for i, j in enumerate(perm):
        result[j] = i

    return result


def encrypt(msg: str, a: int, b: int) -> str:
    perm = compute_permutation(a, b, 0x110000)
    unicodes_msg = str_to_unicodes(msg)

    ciphertext = [0] * len(msg)

    for idx, unicode in enumerate(unicodes_msg):
        ciphertext[idx] = perm[unicode]

    return unicodes_to_str(ciphertext)


def encrypt_optimized(msg: str, a: int, b: int) -> str:

    unicode_msg = str_to_unicodes(msg)
    ciphertext = [0] * len(unicode_msg)

    for idx, unicode in enumerate(unicode_msg):
        ciphertext[idx] = (a * unicode + b) % 0x110000
    return unicodes_to_str(ciphertext)


def decrypt(msg: str, a: int, b: int) -> str:
    perm = compute_inverse_permutation(a, b, 0x110000)
    unicode_msg = str_to_unicodes(msg)

    plaintext = [0] * len(msg)

    for idx, unicode in enumerate(unicode_msg):
        plaintext[idx] = perm[unicode]

    return unicodes_to_str(plaintext)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    unicode_msg = str_to_unicodes(msg)
    plaintext = [0] * len(unicode_msg)

    for idx, unicode in enumerate(unicode_msg):
        plaintext[idx] = a_inverse * (unicode - b) % 0x110000

    return unicodes_to_str(plaintext)


def compute_affine_keys(n: int) -> list[int]:
    return [i for i in range(n) if gcd(i, n) == 1]


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for key in affine_keys:
        if (a * key % n) == 1:
            return key

    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    b = 58
    a_values = compute_affine_keys(0x110000)
    for a in a_values:
        plaintext = decrypt(s, a, b)
        if "bombe" in plaintext:
            return plaintext, (a, b)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    a_values = compute_affine_keys(0x110000)
    for a in a_values:
        a_inverse = compute_affine_key_inverse(a, a_values, 0x110000)
        for b in range(10000):
            plaintext = decrypt_optimized(s, a_inverse, b)
            if "bombe" in plaintext:
                return plaintext, (a, b)

    raise RuntimeError("Failed to attack")
