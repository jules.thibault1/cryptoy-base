from math import (
    gcd,
)

from cryptoy.utils import (
    draw_random_prime,
    int_to_str,
    modular_inverse,
    pow_mod,
    str_to_int,
)


def keygen() -> dict:
    e = 65537
    p = draw_random_prime()
    q = draw_random_prime()
    N = p * q
    phi = (p - 1) * (q - 1)

    d = modular_inverse(e, phi)
    return {"public_key": (e, N), "private_key": d}


def encrypt(msg: str, public_key: tuple) -> int:
    e, N = public_key
    m = str_to_int(msg)

    if m >= N:
        raise ValueError("Message too large for public key")

    return pow_mod(m, e, N)


def decrypt(msg: int, key: dict) -> str:
    d = key["private_key"]
    N = key["public_key"][1]

    m = pow_mod(msg, d, N)
    return int_to_str(m)
