from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César


def encrypt(msg: str, shift: int) -> str:
    return_value = ""
    for c in msg:
        return_value += unicodes_to_str([str_to_unicodes(c)[0] + shift])[0]
    return return_value


def decrypt(msg: str, shift: int) -> str:
    return encrypt(msg, -shift)


def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    for i in range(0x110000):
        d = decrypt(s, i)
        if 'ennemis' in d:
            print("Message dechiffré", d)
            return (d, i)
    raise RuntimeError("Failed to attack")
